#Sets fish greeting to nothing, removes welcome message.
set fish_greeting
# Opens my preferred prompt: starship
starship init fish | source
# Runs a custom pfetch script.
umutfetch
